# Issue Log Graphs

As a preliminary test, here are the topic activity graphs of every month so far (and the total one). Currently there are 2 different stats graphed, [topics per day](#topics-per-day): how many topics were posted for every day in the month and [topics each day](#topics-each-day): how many topics were posted on each day of the week in that month.

## Topics Per Day

This first graph is the total amount of topics of Tildes, divided into weeks instead of days. All the other graphs will be per day.

![Total topics per week](./topics_per_week.png)

Tildes launched on the 26th of April, so there's only a couple days in this graph. :P

![Topics per day: April 2018](./topics_per_day/2018_04.png)

![Topics per day: May 2018](./topics_per_day/2018_05.png)

![Topics per day: June 2018](./topics_per_day/2018_06.png)

![Topics per day: July 2018](./topics_per_day/2018_07.png)

![Topics per day: August 2018](./topics_per_day/2018_08.png)

![Topics per day: September 2018](./topics_per_day/2018_09.png)

![Topics per day: October 2018](./topics_per_day/2018_10.png)

![Topics per day: November 2018](./topics_per_day/2018_11.png)

![Topics per day: December 2018](./topics_per_day/2018_12.png)

![Topics per day: January 2019](./topics_per_day/2019_01.png)

![Topics per day: February 2019](./topics_per_day/2019_02.png)

## Topics Each Day

![Total topics per day](./topics_each_day.png)

Again the first month only has a select number of days to pull from.

![Topics per day: April 2018](./topics_each_day/2018_04.png)

![Topics per day: May 2018](./topics_each_day/2018_05.png)

![Topics per day: June 2018](./topics_each_day/2018_06.png)

![Topics per day: July 2018](./topics_each_day/2018_07.png)

![Topics per day: August 2018](./topics_each_day/2018_08.png)

![Topics per day: September 2018](./topics_each_day/2018_09.png)

![Topics per day: October 2018](./topics_each_day/2018_10.png)

![Topics per day: November 2018](./topics_each_day/2018_11.png)

![Topics per day: December 2018](./topics_each_day/2018_12.png)

![Topics per day: January 2019](./topics_each_day/2019_01.png)

![Topics per day: February 2019](./topics_each_day/2019_02.png)
