# Tildes Issue Log

## 2019

* [February](https://til.bauke.xyz/posts/february-2019.html)
* [January](https://til.bauke.xyz/posts/january-2019.html)

## 2018

* [December](https://til.bauke.xyz/posts/december-2018.html)
* [November](https://til.bauke.xyz/posts/november-2018.html)
* [October](https://til.bauke.xyz/posts/october-2018.html)
* [September](https://til.bauke.xyz/posts/september-2018.html)
* [August](https://til.bauke.xyz/posts/august-2018.html)
* [July](https://til.bauke.xyz/posts/july-2018.html)
* [June](https://til.bauke.xyz/posts/june-2018.html)
* [May](https://til.bauke.xyz/posts/may-2018.html)

## Contributing

If you'd like to write a highlight section about a feature, please do! I'm not a writer by any means and would love to have other people write these instead. If you're not familiar with the Git workflow you can always [private message me](https://tildes.net/user/Bauke/new_message) with what you've written and I'll add your section for you, crediting your Tildes profile. If you'd like to learn Git, you can find tons of guides online. [Here's](https://try.github.io/) an excellent one.

## Building

If you'd like to build the issue table, statistics and/or website yourself you can follow these steps.

You'll need [Node JS](https://nodejs.org/) and [Yarn](https://yarnpkg.com/). You *can* use NPM however I don't. You probably know what you're doing though at that point.

* Clone the repository
  * SSH: `git clone git@gitlab.com:Bauke/tildes-issue-log.git`
  * HTTPS: `git clone https://gitlab.com/Bauke/tildes-issue-log.git`
* Change into the directory: `cd tildes-issue-log`
* Install the dependencies: `yarn`
* Create a `config.json` file from the sample config, including a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) from GitLab. **config.json is gitignored however make sure you don't publish your token somewhere on accident.**
* Run `yarn dl` to download the issues from the current month we're in and build the statistics and issue table, if you already have the issue files you can run `yarn nodl` to skip the downloading
* To easily develop the website itself you can run `yarn watch` to watch the `src/` folder for changes and automatically lint the sass and build the files. Running `yarn watch` will open your default browser at `localhost:3000`.
* To build the site only once you can do `yarn build`, the files will be built under `public/`, a requirement for GitLab Pages.

A new gitignored directory will be created called `data/` including all the files to generate the Statistics and the Issue Table files, which will appear in the "out" folder labelled as `MonthYear_table/statistics.html`. To generate them for a specific year and/or month you'll have to go into the Gulp file and change the `wantedYear` and/or the `wantedMonth`.

If you're working on the generation process you can run `yarn nodl` to skip the downloading phase, this will only generate `table.html` and `statistics.html`. This way you don't have to wait several seconds for the GitLab API to respond.

If you want to lint your sass (using [Stylelint](https://stylelint.io/)) manually you can run `yarn lint`, in the future when we have JavaScript (if ever) this will also include JS linting however as there's 0 JS ending up on the site at the moment there's no need for it.
